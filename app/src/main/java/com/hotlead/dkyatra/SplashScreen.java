package com.hotlead.dkyatra;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;

public class SplashScreen extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getActionBar().hide();
        setContentView(R.layout.activity_splash_screen);
        //getActionBar().setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));


    }
}